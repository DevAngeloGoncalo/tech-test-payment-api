using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Entities
{
    public enum EnumStatusVenda
    {
        Aguardando_Pagamento,
        Pagamento_Aprovado,
        Cancelada,
        Enviado_Transportadora,
        Entregue        
    }
}