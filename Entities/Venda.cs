using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Venda
    {
        [Key]
        public int IdVenda { get; set; }
        public DateTime Data { get; set; }
        public EnumStatusVenda StatusVenda { get; set; }

        public int IdVendedor { get; set; }
        public Vendedor Vendedor { get; set; }

        //public Produto Produto { get; set; }       
    }
}