using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Vendedor
    {
        public Vendedor()
        {
            Vendas = new List<Venda>();
        }

        [Key]
        public int IdVendedor { get; set; }
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public List<Venda> Vendas { get; set; }
    }
}