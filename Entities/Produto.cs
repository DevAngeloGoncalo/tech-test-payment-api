using System.ComponentModel.DataAnnotations;

namespace tech_test_payment_api.Entities
{
    public class Produto
    {
        [Key]
        public int IdProduto { get; set; }
        public string Nome { get; set; }
        public decimal Preco { get; set; }
        public int IdVenda { get; set; }
    }
}